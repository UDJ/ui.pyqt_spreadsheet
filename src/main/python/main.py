import sys
import os
import csv
from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5 import QtCore, QtGui, QtWidgets



class NewTable(QtWidgets.QTableWidget):
    def __init__(self, parent=None):
        super(NewTable, self).__init__(1, 1, parent)
        self.setFont(QtGui.QFont("Helvetica", 10, QtGui.QFont.Normal, italic=False))   
        self.horizontalHeader().setHighlightSections(False)
        self.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Fixed)
        
        self.setSelectionMode(QtWidgets.QAbstractItemView.NoSelection)
        self.setColumnWidth(0, 130)

        self.cellChanged.connect(self.edit_cell)

    @QtCore.pyqtSlot(int, int)
    def edit_cell(self, r, c):
        it = self.item(r, c)
        it.setTextAlignment(QtCore.Qt.AlignCenter)        

    @QtCore.pyqtSlot()
    def  new_row(self):
        columncount = self.columnCount()
        self.insertColumn(columncount)

    @QtCore.pyqtSlot()
    def new_column(self):
        rowcount = self.rowCount()
        self.insertRow(rowcount)
    

    @QtCore.pyqtSlot()
    def save_table_csv(self):
        path = QtWidgets.QFileDialog.getSaveFileName(self,  'Save CSV',  os.getenv('HOME'),  'CSV(*.csv')
        if path[0] != '':
            with open(path[0],  'w') as csv_file:
                write = csv.writer(csv_file)
                for row in range(self.rowCount()):
                    row_data = []
                    for column in range(self.columnCount()):
                        item = self.item(row,  column)
                        if item is not None:
                            row_data.append(item.text())
                        else:
                            row_data.append('')
                    write.writerow(row_data)


class NewTableCreate(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(NewTableCreate, self).__init__(parent)    

        table = NewTable()

        new_row_button = QtWidgets.QPushButton("New Row")
        new_row_button.clicked.connect(table.new_row)

        new_column_button = QtWidgets.QPushButton("New Column")
        new_column_button.clicked.connect(table.new_column)

        save_button = QtWidgets.QPushButton("Save")
        save_button.clicked.connect(table.save_table_csv)

        button_layout = QtWidgets.QVBoxLayout()
        button_layout.addWidget(save_button, alignment=QtCore.Qt.AlignTop)
        button_layout.addWidget(new_row_button, alignment=QtCore.Qt.AlignBottom)
        button_layout.addWidget(new_column_button, alignment=QtCore.Qt.AlignBottom)

        tablehbox = QtWidgets.QVBoxLayout()
        tablehbox.addWidget(table)

        grid = QtWidgets.QGridLayout(self)
        grid.addLayout(button_layout, 0, 1)
        grid.addLayout(tablehbox, 0, 0)  


if __name__ == '__main__':
    appctxt = ApplicationContext()          # 1. Instantiate
    
    app = QtWidgets.QApplication([])
    window = NewTableCreate()
    window.show()
    
    exit_code = appctxt.app.exec_()      # 2. Invoke appctxt.app.exec_()
    sys.exit(exit_code)
